<?php
	$con 	= mysqli_connect("localhost","root","","scada");

	$url    = dirname(__DIR__).'/scada-parser-csv/';
	$dirs 	= scandir($url);

	foreach ($dirs as $dir) {

		if ($dir == 'IJA0101-00007779') {
			$dirnow = $url.$dir.'/DATA/';
			$subdirs = scandir($dirnow);

			if (!file_exists($dirnow.'ARCHIVE')) {
			    mkdir($dirnow.'ARCHIVE', 0777, true);
			}

			foreach ($subdirs as $subdir) {
				// Pressure & Level
				if (substr($subdir, 0, 5)=='m_cu_' && substr($subdir, -3)=='csv') {
					$subdirnow = $dirnow.$subdir;
					$data_array = [];

					if(($handler = fopen("{$subdirnow}", "r")) !== FALSE){
						while(($data = fgetcsv($handler,1000,","))!==FALSE){
							$data_array[] = $data;
						}

						for($i=1; $i<count($data_array); $i++){
							$raw_tanggal	= str_replace('/', '-', $data_array[$i][0]);
							$tanggal 		= substr($raw_tanggal, 0, 10);
							$jam 			= substr($raw_tanggal, -8);
							$logger_id		= $data_array[$i][1];
							// $totalizer 		= $data_array[$i][2];
							$flowrate		= $data_array[$i][3];

							// mysqli_query($con, "INSERT INTO totalizers (logger_id, tanggal, jam, totalizer, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$totalizer', '$raw_tanggal', '$raw_tanggal')");
									
							mysqli_query($con, "INSERT INTO flowrates (logger_id, tanggal, jam, flowrate, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$flowrate', '$raw_tanggal', '$raw_tanggal')");		
						}

						// Tutup file yang telah dibuka
						fclose($handler);

						// Selesai disimpan ke database, pindahkan file ke folder arsip
						rename($subdirnow, $dirnow.'ARCHIVE/'.$subdir);
					}
				}
			}			

		}else{
			if (substr($dir, 0, 7)=='IJA0101') {
				$dirnow = $url.$dir.'/DATA/';
				$subdirs = scandir($dirnow);

				if (!file_exists($dirnow.'ARCHIVE')) {
				    mkdir($dirnow.'ARCHIVE', 0777, true);
				}

				foreach ($subdirs as $subdir) {
					// Pressure & Level
					if (substr($subdir, 0, 5)=='m_cu_' && substr($subdir, -3)=='csv') {
						$subdirnow = $dirnow.$subdir;
						$data_array = [];

						if(($handler = fopen("{$subdirnow}", "r")) !== FALSE){
							while(($data = fgetcsv($handler,1000,","))!==FALSE){
								$data_array[] = $data;
							}

							for($i=1; $i<count($data_array); $i++){
								$raw_tanggal	= str_replace('/', '-', $data_array[$i][0]);
								$tanggal 		= substr($raw_tanggal, 0, 10);
								$jam 			= substr($raw_tanggal, -8);
								$logger_id		= $data_array[$i][1];

								if(substr($data_array[0][3], -3)=='bar'){
									$pressure 	= $data_array[$i][3];
									$level 		= 0;
								}else{
									$pressure 	= 0;
									$level 		= $data_array[$i][3];
								}

								// Pressure
								mysqli_query($con, "INSERT INTO pressures (logger_id, tanggal, jam, pressure, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$pressure', '$raw_tanggal', '$raw_tanggal')");

								// Level
								mysqli_query($con, "INSERT INTO levels (logger_id, tanggal, jam, level, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$level', '$raw_tanggal', '$raw_tanggal')");		
							}

							// Tutup file yang telah dibuka
							fclose($handler);

							// Selesai disimpan ke database, pindahkan file ke folder arsip
							rename($subdirnow, $dirnow.'ARCHIVE/'.$subdir);
						}
					}

					// Totalizer & Flowrate
					if (substr($subdir, 0, 7)=='m_cnt1_' && substr($subdir, -3)=='csv') {
						$subdirnow = $dirnow.$subdir;
						$data_array = [];

						if(($handler = fopen("{$subdirnow}", "r")) !== FALSE){
							while(($data = fgetcsv($handler,1000,","))!==FALSE){
								$data_array[] = $data;
							}

							for($i=1; $i<count($data_array); $i++){
								$raw_tanggal	= str_replace('/', '-', $data_array[$i][0]);
								$tanggal 		= substr($raw_tanggal, 0, 10);
								$jam 			= substr($raw_tanggal, -8);
								$logger_id		= $data_array[$i][1];
								$totalizer 		= $data_array[$i][2];
								$flowrate		= $data_array[$i][3]*1000;

								mysqli_query($con, "INSERT INTO totalizers (logger_id, tanggal, jam, totalizer, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$totalizer', '$raw_tanggal', '$raw_tanggal')");		
								mysqli_query($con, "INSERT INTO flowrates (logger_id, tanggal, jam, flowrate, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$flowrate', '$raw_tanggal', '$raw_tanggal')");		
							}

							// Tutup file yang telah dibuka
							fclose($handler);

							// Selesai disimpan ke database, pindahkan file ke folder arsip
							rename($subdirnow, $dirnow.'ARCHIVE/'.$subdir);
						}
					}

					// Signal & Baterai
					if (substr($subdir, 0, 7)=='m_diag_' && substr($subdir, -3)=='csv') {
						$subdirnow = $dirnow.$subdir;
						$data_array = [];

						if(($handler = fopen("{$subdirnow}", "r")) !== FALSE){
							while(($data = fgetcsv($handler,1000,","))!==FALSE){
								$data_array[] = $data;
							}

							for($i=1; $i<count($data_array); $i++){
								$raw_tanggal	= str_replace('/', '-', $data_array[$i][0]);
								$tanggal 		= substr($raw_tanggal, 0, 10);
								$jam 			= substr($raw_tanggal, -8);
								$logger_id		= $data_array[$i][1];
								$baterai 		= $data_array[$i][3];
								$sinyal			= $data_array[$i][4];

								// echo $baterai.'|'.$sinyal.'<br>';

								mysqli_query($con, "INSERT INTO batteries (logger_id, tanggal, jam, baterai, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$baterai', '$raw_tanggal', '$raw_tanggal')");		
								mysqli_query($con, "INSERT INTO signals (logger_id, tanggal, jam, sinyal, created_at, updated_at) VALUES('$logger_id', '$tanggal', '$jam', '$sinyal', '$raw_tanggal', '$raw_tanggal')");		
							}

							// Tutup file yang telah dibuka
							fclose($handler);

							// Selesai disimpan ke database, pindahkan file ke folder arsip
							rename($subdirnow, $dirnow.'ARCHIVE/'.$subdir);
						}
					}
				}
				// echo '<hr>';
			}
		}
	}

	mysqli_close($con);